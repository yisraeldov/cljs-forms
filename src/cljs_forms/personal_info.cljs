(ns cljs-forms.personal-info
  (:require [cljs-forms.form-components
             :refer
             [input-field map-questions submit-button]]
            [reagent.core :as reagent :refer [atom]]))

(defonce form-state (atom {:location nil}))

(def questions
  {:email                [input-field
                          {:label    :email
                           :type     "email"
                           :required true
                           :pattern  "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,14}$"}]
   :gender               [{:label    :genre
                           :required true}
                          :male :female]
   :age                  [{:label    :age
                           :required true}
                          :f-age1824 :f-age2534 :f-age3544 :f-age4555 :f-age55]
   :most-often-treatment [{:label    :products
                           :multiple true
                           :limit    3}
                          :day-treatment
                          :night-treatment
                          :serum
                          :eye-contour-treatment]})

(defn form [options]
  [:form options
   (map-questions questions form-state)
   (submit-button :next)])



