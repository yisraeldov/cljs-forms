(ns cljs-forms.i18n
  (:require [reagent.core :as reagent :refer [atom]]
            [taoensso.tempura :as tempura]))

(defonce locales (atom [:en]))

;;add translations here
(def max45-dictonary
  {:en
   {;login tab
    :operator-id "Operator ID"

                                        ;personal info tab
    :email                 "Email address"
    :genre                 "You are?"
    :male                  "Man"
    :female                "Woman"
    :age                   "Your age?"
    :f-age1824             "18-24"
    :f-age2534             "25-34"
    :f-age3544             "35-44"
    :f-age4555             "45-55"
    :f-age55               "55+"
    :products              "Which facial products are you using?"
    :day-treatment         "Day Treatment"
    :night-treatment       "Night Treatment"
    :serum                 "Serum"
    :eye-contour-treatment "Eye Contour Treatment"

                                        ;custom cream tab
    :day-or-night       "Are you interested in day cream, night cream?"
    :f-usemorning	"Day"
    :f-useevening	"Night"
    :external-factors	"What external factors are you particularly exposed to?"
    :f-factorsun	"Sun"
    :f-factorstress	"Stress"
    :f-factorpollution	"Pollution"
    :f-factoraircond	"Excessive heating or Air conditioning"
    :skin-type		"What is your skin type?"
    :f-skinnormal	"Normal"
    :f-skindry		"Dry"
    :f-skinoily		"Oily"
    :f-skincombination	"Combination"
    :concerns           "What do you like to improve?"
    :f-concernsensitive	"Skin sensitivity"
    :f-concernredness	"Redness"
    :f-concernwrinkles	"Wrinkles"
    :f-concernfinelines	"Fine lines"
    :f-concernaging	"Aging & firming"
    :f-concerndarkspots	"Dark Spots"
    :f-concernexsebum	"Excess of Sebum"
    :f-concernacne	"Acne"
    :f-concerndullskin	"Dull complexion"
    :improve-eye	"What do you like to improve at the eye contours?"
    :f-eyedarkcircles   "Dark Circles"
    :f-eyepufiness	"Puffiness"
    :f-eyecrowsfeet     "Crow's feet wrinkle"

                                        ;scan-tab	

                                        ;Analysis -tab
    :s-darkcircles       "Dark Circles"
    :s-wrinkles          "Wrinkles"
    :s-hyperpigmentation "Hyper-pigmentation"
    :s-redness           "Redness"
    :s-eyebags           "Eye-bags"
    :s-texture           "Texture"
    :s-shine             "Shine"
    :s-smoothness        "Smoothness"

                                        ;bar
    :tab {:login           "Login"
          :personal-info   "Personal Info"
          :customize-cream "Customize OrLab Cream"
          :scan            "Scan"
          :analysis        "Analysis"
          :order-cream     "Order Cream"
          :done            "Done"}

                                        ;navigation
    :next                "Next"
    :please-select-up-to "Please select up to"
    :required            "required"}

   :fr
   {;login tab
    :operator-id "Identifiant Salarié"

                                        ;personal info tab
    :email                 "Votre adresse Email "
    :genre                 "Vous êtes ?"
    :male                  "Homme "
    :female                "Femme "
    :age                   "Votre âge?"
    :f-age1824             "18-24"
    :f-age2534             "25-34"
    :f-age3544             "35-44"
    :f-age4555             "45-55"
    :f-age55               "55+"
    :products              "Quels produits pour le visage utilisez-vous?"
    :day-treatment         "	Soin de Jour "
    :night-treatment       "Soin de Nuit"
    :serum                 "Sérum"
    :eye-contour-treatment "Traitement contour des yeux"

                                        ;custom cream tab
    :day-or-night       "Etes-vous interessé(e) par une crème de Jour,  crème de nuit?"
    :f-usemorning	"Jour"
    :f-useevening	"Nuit"
    :external-factors	"A quels facteurs externes êtes-vous particulièrement exposé?"
    :f-factorsun	"Soleil"
    :f-factorstress	"Stress"
    :f-factorpollution	"Pollution"
    :f-factoraircond	"Air conditionné ou chaleur excessive"
    :skin-type		"Quel est vôtre type de peau ?"
    :f-skinnormal	"Normale"
    :f-skindry		"Sèche"
    :f-skinoily		"Grasse"
    :f-skincombination	"Mixte"
    :concerns           "Que souhaitez-vous améliorer?"
    :f-concernsensitive	"Peau Fragile"
    :f-concernredness	"Rougeur Diffuse"
    :f-concernwrinkles	"Rides"
    :f-concernfinelines	"Ridules d'expressions"
    :f-concernaging	"Fermeté et peau mature"
    :f-concerndarkspots	"Tâches pigmentaires"
    :f-concernexsebum	"Excès de Sébum "
    :f-concernacne	"Acné"
    :f-concerndullskin	"Teint Terne "
    :improve-eye	"Que souhaitez-vous améliorer sur votre contour des yeux?"
    :f-eyedarkcircles   "Cernes"
    :f-eyepufiness	"Poches"
    :f-eyecrowsfeet     "Pattes d'Oies"

                                        ;scan-tab	

                                        ;Analysis -tab
    :s-darkcircles       "Cernes "
    :s-wrinkles          "Rides"
    :s-hyperpigmentation "Hyper-Pigmentation "
    :s-redness           "Rougeurs"
    :s-eyebags           "Poches sous les Yeux"
    :s-texture           "Texture"
    :s-shine             "Brillance"
    :s-smoothness        "Douceur"

                                        ;bar
    :tab {:login           "Connectez-vous "
          :personal-info   "Informations personnelles"
          :customize-cream "Crème Orlab Personalisée"
          :scan            "Scan"
          :analysis        "Analyse"
          :order-cream     "Commencer le processus de fabrication"}

                                        ;navigation
    :next                "Suivant"
    :please-select-up-to "Sélèctionnez jusqu'à"
    :required            "Obligatoire"}

   :he
   {:text-dir    "RTL"
                                        ;login tab
    :operator-id ""

                                        ;personal info tab
    :email                 "כתובת מייל"
    :genre                 "הנך:"
    :male                  "זכר"
    :female                "נקבה"
    :age                   "גילך:"
    :f-age1824             "18-24"
    :f-age2534             "25-34"
    :f-age3544             "35-44"
    :f-age4555             "45-55"
    :f-age55               "55+"
    :products              "באילו מוצרי קוסמטיקה לפנים את/ה משתמש/ת?"
    :day-treatment         "קרם יום"
    :night-treatment       "קרם לילה"
    :serum                 "סרום"
    :eye-contour-treatment "קרם עיניים"

                                        ;custom cream tab
    :day-or-night       "האם את/ה מתעניינ/ת בקרם יום או קרם לילה?"
    :f-usemorning	"קרם יום"
    :f-useevening	"קרם לילה"
    :external-factors	"לאילו גורמים חיצוניים את/ה חשופ/ה?"
    :f-factorsun	"שמש"
    :f-factorstress	"לחץ"
    :f-factorpollution	"זיהום אויר"
    :f-factoraircond	"חימום יתר או מזגן"
    :skin-type		"מהו סוג העור שלך?"
    :f-skinnormal	"רגיל"
    :f-skindry		"יבש"
    :f-skinoily		"שומני"
    :f-skincombination	"משולב"
    :concerns           "מה היית רוצה לשפר?"
    :f-concernsensitive	"רגישות העור"
    :f-concernredness	"אדמומיות"
    :f-concernwrinkles	"קמטים"
    :f-concernfinelines	"קמטי הבעה"
    :f-concernaging	"הזדקנות ומיצוק"
    :f-concerndarkspots	"כהויות"
    :f-concernexsebum	"מקומות מבריקים יתר על המידה"
    :f-concernacne	"אקנה"
    :f-concerndullskin	"עור יבשושי"
    :improve-eye	"מה היית רוצה לשפר ביחס לעיניך?"
    :f-eyedarkcircles   "כהויות"
    :f-eyepufiness	"נפיחות מתחת לעיניים"
    :f-eyecrowsfeet     "קמטי עיניים"

                                        ;scan-tab	

                                        ;Analysis -tab
    :s-darkcircles       "עיגולים כהים"
    :s-wrinkles          "קמטים"
    :s-hyperpigmentation "פיגמנטציה"
    :s-redness           "אדמומיות"
    :s-eyebags           "שקיות עיניים"
    :s-texture           "מרקם העור"
    :s-shine             "ברק העור"
    :s-smoothness        "חלקות"

                                        ;bar
    :tab {:login           "התחבר"
          :personal-info   "פרטים אישיים"
          :customize-cream "התאמת קרם אורלאב"
          :scan            "סריקה"
          :analysis        "ניתוח נתונים"
          :order-cream     "הזמן קרם"}

                                        ;navigation
    :next                "הבא"
    :please-select-up-to "בבקשה בחר עד "
    :required            "נדרש"}})

;;don't edit below this point
(def opts {:dict max45-dictonary})

(defn tr
  ([resource-ids] (taoensso.tempura/tr opts  @locales resource-ids))
  ([resource-ids resource-args] (taoensso.tempura/tr opts  @locales resource-ids resource-args))) ; You'll typically use a partial like this

(defn lang-menu [langs]
  [:div#language-menu.dropdown
   [:a.btn.btn-link.dropdown-toggle
    {:tab-index "0"}
    (langs (first @locales)) [:i.icon.icon-caret]]
   [:ul.menu
    (map (fn [[key name]] ^{:key (str "lang-" name)}
           [:li.menu-item
            [:a {:href "#"
                 :on-click (fn[]
                             (reset! locales [key]))
                 } name]])
         langs)]])

(def language-selector
  (lang-menu {:en "English"
              :he "עברית"
              :fr "Francois"}))

