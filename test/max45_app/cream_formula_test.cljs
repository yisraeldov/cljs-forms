(ns cljs-forms.cream-formula-test
  (:require  [cljs.test :as t :include-macros true :refer [deftest is testing]]
             [cljs-forms.cream-formula :refer [form->factors]]))

        
(deftest form->factors-test
  (testing "Testing from->factors"
      (is (= (form->factors {:field :value}) {:value 1}))
      (is (= (form->factors {:field [:sub-value]}) {:sub-value 1}))
      (is (= (form->factors {:a [:a1 :a2] :b :b1 }) {:a1 1 :a2 1 :b1 1}))
      (is (= (form->factors {:a 50}) {:a 50}) "number value ")
      (is (= (form->factors {:a [:a1 :a2] :b :b1 :c 0.3})
             {:a1 1 :a2 1 :b1 1 :c 0.3}))))


