(ns cljs-forms.cream-formula
  [:require [formula.formula :as formula]])

(defn form->factors [form-data]
  (into {} (map (fn [[key value]]
                  (cond (or (vector? value) (set? value))
                        (into {} (map (fn [sub-value] {sub-value 1}) value))
                        (number? value) {key value}
                  :else {value 1})) form-data)))

(defn cream-ingredients [form-data scan-data ingredients]
  (into {} (remove (fn [[_ v]] (nil? v))
                  (let [factor-levels (into scan-data (form->factors form-data))]
                    (formula.formula/make-normalized-formula ingredients factor-levels 10)))))

