(ns cljs-forms.form-components
  [:require [cljs-forms.i18n :as i18n :refer [tr]]])

(defn state-updater [form-state field-key]
  (fn [event] (swap! form-state assoc field-key (.-value (.-target event)))))

(defn button-option-group [options state state-key
                           & {:keys [multiple]}]
  [:div.btn-group.btn-group-block
   (doall (map (fn [option]
                 ^{:key (str state-key "button-group-option-" option)}
                 [:button.btn
                  {:type "button"
                   :class
                   (when (or
                        (= (state-key @state) option)
                        (and multiple (contains? (state-key @state) option))) "active")
                   :on-click (if multiple
                               (fn []
                                 (let [update-method (if (contains? (state-key @state) option) disj conj)]
                                   (swap! state update state-key (comp  (partial into #{}) update-method) option)))
                               (fn [] (swap! state assoc state-key option) nil))}
                  option])
               options))])

(defn option-group
  ([state state-key options]
   (option-group {} state state-key options))
  ([element-options state state-key options]
   (let [multiple (element-options :multiple)
         required (element-options :required)
         limit    (element-options :limit)]
     [:div.form-group element-options
      (when limit [:div [:em (tr [:please-select-up-to "Please select up to "]) [:strong limit]]])
      (doall (map (fn [option]
                    (let [checked (if (or
                                       (= (state-key @state) option)
                                       (and multiple (contains? (state-key @state) option))) true false)]
                      ^{:key (str state-key "group-option-" option)}
                      [:label.form-inline.m-2
                       {:class (if multiple "form-checkbox" "form-radio")}
                       [:input
                        {:type (if multiple "checkbox" "radio")
                         :required (and required (or (not multiple) (= 0 (count (@state state-key)))))
                         :name state-key
                         :disabled (and  multiple (not checked) (<= limit (count (@state state-key))))
                         :checked checked
                         :on-change (if multiple
                                      (fn []
                                        (let [update-method (if checked disj conj)]
                                          (swap! state update state-key (comp  (partial into #{}) update-method) option)))
                                      (fn [] (swap! state assoc state-key option) nil))}]
                       [:i.form-icon] (tr [option])]))
                  options))])))

(defn input-field
  ([form-state field-key]
   (input-field {} form-state field-key))
  ([options form-state field-key]
   [:input.form-input.input-sm
    (merge {:on-change (state-updater form-state field-key)
            :value (@form-state field-key)
            } options)]))

(defn submit-button
  ([title]
   (submit-button {} title))
  ([options & title]
   [:button.btn.input-group-btn.btn-primary options (tr (into [] title))]))

(defn form-group
  ([options form-element-method form-state state-key & method-params]
   (form-group options
               (apply form-element-method options form-state state-key method-params)))
  ([options form-element]
   [:div.form-group options
    [:label.form-label (tr [(get options :label)])
     (when (:required options)
           [:span " - "[:em (tr [:required "required"])]] )]
    form-element]))

(defn map-questions [questions form-state]
  (doall (map (fn [[question-key question]]
                (with-meta
                  (if (map? (first question))
                    [form-group (first question)
                     option-group form-state question-key (rest question)]
                    (let [options                (nth question 1)
                          form-element-method    (first question)
                          element-method-params (nthrest question 2)]
                      (apply form-group
                             options
                             form-element-method
                             form-state
                             question-key
                             element-method-params)))
                  {:key question-key}))
              questions)))


