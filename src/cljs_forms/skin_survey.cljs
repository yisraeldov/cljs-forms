(ns cljs-forms.skin-survey
  (:require [cljs-forms.form-components :refer [map-questions submit-button]]
            [reagent.core :as reagent :refer [atom]]))

(defonce form-state (atom {}))

(def questions
  {:day-or-night            [{:label    :day-or-night
                              :required true}
                             :f-usemorning
                             :f-useevening]
   :external-factors        [{:label    :external-factors
                              :multiple true
                              :limit    2}
                             :f-factorsun
                             :f-factorstress
                             :f-factorpollution
                             :f-factoraircond]
   :Skin-type               [{:label    :skin-type	
                              :required true}
                             :f-skinnormal
                             :f-skindry	
                             :f-skinoily
                             :f-skincombination]
   :first-factor-to-improve [{:label    :concerns
                              :multiple true
                              :limit    4}
                             :f-concernsensitive
                             :f-concernredness
                             :f-concernwrinkles
                             :f-concernfinelines
                             :f-concernaging
                             :f-concerndarkspots
                             :f-concernexsebum
                             :f-concernacne
                             :f-concerndullskin]
   :skin-eyes               [{:label    :improve-eye
                              :multiple true
                              :limit    3}
                             :f-eyedarkcircles
                             :f-eyepufiness
                             :f-eyecrowsfeet]})

(defn form [options]
  [:form options
   (map-questions questions form-state)
   (submit-button :next)])
