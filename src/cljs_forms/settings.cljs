(ns cljs-forms.settings
  (:require [cljs-forms.form-components :refer [form-group input-field]]))

(defn form [options settings]
  [:form options
   (form-group {:label "url of device"}
               (input-field settings :machine-url))
   (form-group {:label "url of formula ingredients"}
               (input-field settings :ingredients-url))])
