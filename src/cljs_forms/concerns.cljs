(ns cljs-forms.concerns
  (:require
   [cljs-forms.form-components :refer [form-group, option-group]]
   [reagent.core :as reagent :refer [atom]]))

(defonce form-state (atom {}))

(defn skin-type-selector []
  [form-group
   {:label "What is your skin type?"}
   (option-group  {:required true} form-state :skin-type  ["Normal" "Dry" "Oily" "Combination"])])

(defn additional-concerns-selector []
  [:div.form-group
   [:label.form-label "Do you have any personal skin concerns? Check all that apply."]
   (option-group
    {:multiple true :limit 5 :required true}
    form-state :personal-concerns
    ["Wrinkles & fine lines"
     "Eyebags"
     "Redness"
     "Sensitive"
     "Aging & firming"
     "Dull Skin"
     "Pollution"
     "Skin Breakouts"
     "Acne"
     "Dark Spots"
     "Excess of Sebum"])])

(defn form [options]
  [:div
   [:form options
    [:h2 "Skin Type and Concerns"]
    (skin-type-selector)
    (additional-concerns-selector)
    [:button.btn.input-group-btn.btn-primary "Next"]]])
