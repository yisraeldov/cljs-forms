(ns ^:figwheel-hooks cljs-forms.core
  (:require [ajax.core :refer [GET]]
            ajax.easy
            ajax.edn
            [alandipert.storage-atom :as storage-atom :refer [local-storage]]
            [clojure.string :as str]
            [formula.formula :as formula]
            [goog.dom :as gdom]
            [cljs-forms.cream-formula :refer [cream-ingredients]]
            [cljs-forms.driver :as driver]
            [cljs-forms.i18n :as i18n :refer [tr]]
            [cljs-forms.login :as login]
            [cljs-forms.personal-info :as personal-info]
            [cljs-forms.revieve :as Revieve :refer [revieve]]
            [cljs-forms.settings :as settings]
            [cljs-forms.skin-survey :as skin-survey]
            [reagent.core :as reagent :refer [atom]]))

;; define your app data so that it doesn't get over-written on reload
(defonce app-state (atom
                    {:text "MAX45 prototype"
                     :image nil
                     :analyzing nil
                     :results nil
                     :visable-tab :tab/login
                     :tab-history []
                     :active-step nil
                     :pump-status {}
                     :debug false
                     }))

(add-watch app-state :rollbar-watcher
           (fn [_ _ _ new-state]
             (js/Rollbar.captureEvent (clj->js {:new-state new-state}) "info")))


(defonce app-settings (local-storage
                   (atom
                    {:machine-url "http://orlab.ydns.eu:5000/" ;;in production shoud be name of the pi
                     :ingredients-url "http://formula.max45.com/master/data/ingredients.edn"})
                   :settings))

(add-watch app-state :update-personal-info
           (fn [_ _ old-state new-state]
             (when (not= (:personal-info old-state) (:personal-info new-state))
               (driver/send-personal-info (:personal-info new-state)  (:machine-url @app-settings) print)
               (println "new personal info"))))

(add-watch app-state :update-api-state
           (fn [_ _ old-state new-state]
             (when (not= (:visable-tab new-state) (:visable-tab old-state))
               (driver/send-app-state
                ;; Replace the image with the revive image
                (assoc new-state :image (Revieve/get-image))
                (:machine-url @app-settings) print))
             (println "new app-state")))

(add-watch app-state :previous-tab
           (fn [_ _ old-state new-state]
             (when (not= (:visable-tab old-state) (:visable-tab new-state))
               (swap! app-state assoc :tab-history (conj (:tab-history @app-state) (:visable-tab old-state))))))


(defn add-error [error]
  (js/Rollbar.error error)
  (let [errors (:errors @app-state)]
    (swap! app-state assoc :errors (conj errors (str error)))))

(reset! ajax.easy/default-error-handler
        (fn [response]
          (js/Rollbar.captureEvent (clj->js response))
          (add-error (tr ["A communication error, please contact support"]))
          (ajax.easy/print-error-response response)))

;; This tests connectivity and resets the app
(ajax.core/GET (str (:machine-url @app-settings) "/reset"))


(GET (:ingredients-url @app-settings)
     {:handler (fn [response] (swap! app-state assoc :formula-ingredients response) )
      :response-format (ajax.edn/edn-response-format)} )

(def admin-id "adminmax")

(defn admin? []
  (= admin-id (get-in @app-state [:login :operator-id])))
(defn debug? []
    (:debug @app-state))
(defn get-app-element []
  (gdom/getElement "app"))

(defn update-results []
  (swap! app-state assoc :results
         {:s-darkcircles (.getDarkcircles Revieve/CV)
          :s-wrinkles (.getWrinkles Revieve/CV)
          :s-redness (.getRedness Revieve/CV)
          :s-eyebags (.getEyebags Revieve/CV)
          :s-texture (.getTexture Revieve/CV)
          :s-smoothness (.getSmoothness Revieve/CV)
          :s-shine (.getSkinShine Revieve/CV)})
  (swap! app-state assoc :cream-ingredients
         (cream-ingredients
          (merge
           (:personal-info @app-state)
           (:skin-survey @app-state))
          (:results @app-state)
          (:formula-ingredients @app-state)))
  (.reset (.-AR revieve)))

(defn analyze-done []
  (swap! app-state assoc :analyzing nil)
  (swap! app-state assoc :visable-tab :tab/analysis)
  (update-results))

(defn analyze-error [e]
  (swap! app-state assoc :analyzing nil)
  (swap! app-state assoc :image nil)
  (add-error (str e)))

(defn analyze-image []
  (swap! app-state assoc :analyzing true)
  (Revieve/analyze analyze-done analyze-error))

(defn update-image [new-image]
  (swap!  app-state assoc :image new-image)
  (Revieve/set-image (:image @app-state) analyze-image))

(defn image-input []
  [:div.form-group
   [:label.form-label {:for "inputFile"} "Take Picture" [:h3 "📷"]]
   [:input.form-input.input-sm
    (hash-map :type "file" :id "inputFile" :accept "image/*" :capture "user"
              :autoFocus true
              :on-focus (fn [event]
                          (when (and
                                 (nil? (:image @app-state))
                                 (empty? (:errors @app-state))
                                 (empty? (.-value (.-target event))))
                            (.blur (.-target event))
                            (.click (.-target event))))
              :on-change (fn [event]
                           (update-image (.item (.-files (.-target event)) 0))))]])


(defn analyze-button []
  [:button.btn.input-group-btn.btn-primary
   {
    :disabled (or (:analyzing @app-state) (not (:image @app-state)))
    :on-click analyze-image}
   "analyze"
   ])

(defn apply-highlight-method [highlight-method]
  (.setContainer (.-AR revieve) "afterContainer")
  (.dehighlightAll (.-AR revieve))
  (js-invoke (.-AR revieve) highlight-method))


(defn spec-bar [value]
  [:div.bar
     [:div.bar-item {:style {:width (str
                                     (* 100 value) "%")}} (str (int (* 100 value )) "%")]])

(defn analysis-result-card [description value highlight-method]
  [:div.card
   [:div.card-header
    [:div.card-title.h5
     [:a {:on-click (fn [](apply-highlight-method highlight-method))}
      description]]]
   [:div.card-body (spec-bar value)]])


(defn analysis-result-menu-item [description scan-results highlight-method]
  ^{:key (str "scan-menu-" highlight-method)}
  [:li.menu-item
   ;; This is a hack to prevent the fist click from being too large
   ;; TODO find a real fix for issue #41
   {:on-click
    (fn []
      (dotimes [_ 2]
        (apply-highlight-method highlight-method)))}
   [:a (tr [description])]
   (spec-bar (description scan-results))])

(defn split-camel-case [camel-word]
  (clojure.string/join " " (re-seq #"[A-Z]?[^A-Z]+" camel-word) ))

(defn highlight-button [highlight-method results? analyzing?]
  ^{:key highlight-method}
  [:button.btn.input-group-btn
   {:on-click (fn [] (apply-highlight-method highlight-method))
    :disabled (or (not results?) analyzing?)}
   (split-camel-case
    (clojure.string/replace-first  highlight-method #"^highlight" ""))
   ])

(defn tab-button [tab-name]
  ^{:key (str "tab-button-" tab-name)}
  [:li.step-item {:class (when (= (:visable-tab @app-state) tab-name) "active")}
   [:a {:on-click (when (admin?) (fn [] (swap! app-state assoc :visable-tab tab-name)))}
    (tr [tab-name])]])

(defn reset-button
  ([] (reset-button {}))
  ([options]
   [:button.btn
    (merge
     {:class "btn-primary"}
     {:on-click (fn [] (js/window.location.reload  nil))}
     options)
    (tr [:reset [:i.icon.icon-refresh]])]))

(defn get-active-step []
  (get @app-state :active-step))

(defn set-active-step [step]
  (swap! app-state assoc :active-step
         (cond
           (= "done"  step) "done"
           (= "waiting for container" step) "place"
           (str/starts-with? (str step) "Running ") "dosing"
           :else "idle")))

(defn update-status [status]
  (swap! app-state assoc :pump-status status)
  (when (str/starts-with? (status :status) "Error")
    (throw (status :status)))
  (set-active-step (status :status)))

(defn step-item [step-name,step-description]
  [:li.step-item
   {:id (str "step-" step-name)
    :class (when (= (get-active-step) step-name) "active")}
   [:a step-description]])

(defn switch-tab [next-tab]
  (swap! app-state assoc :visable-tab next-tab))

(defn submit-and-next-tab [form-key form-state next-tab]
  (fn [event]
    (.preventDefault  event)
    (print "submited and moving to tab" next-tab)
    (swap! app-state assoc form-key form-state)
    (switch-tab next-tab)))

(defn selected-tab? [tab-name]
  (= (:visable-tab @app-state) tab-name))

(defn choose-image-tab [] [:div
                       (when (:analyzing @app-state)
                         [:div.modal.active
                          [:a.modal-overlay]
                          [:div.modal-container
                           [:div.modal-header [:h1 "We are loading..."]]
                           [:div.modal-body
                            [:div.loading.loading-lg]
                            [:p "Processing your image, please wait a few seconds."]]]])
                       [:div.column
                        [:div.empty
                         (if (:image @app-state)
                           [:figure.avatar.avatar-xl
                            [:img.img-responsive {:src (js/URL.createObjectURL (:image @app-state))}]]
                           [:div.empty-icon
                            [:i.icon.icon-people.icon-3x]
                            [:p.empty-title "Upload Selfie"]])
                         [:div.empty-action
                          (image-input)
                          (analyze-button)]]]])

(defn render-results-tab []
  [:div#results
   [:div.columns
    [:div.column.col-4
     (conj
      (into
       [:ul.menu
        (doall
         (map
               (fn [[key an-method]]
                 (analysis-result-menu-item key (:results @app-state) an-method))
               {:s-darkcircles "highlightDarkcircles"
                :s-wrinkles    "highlightWrinklesIndividually"
                :s-redness     "highlightRedness"
                :s-eyebags     "highlightEyebags"
                :s-texture     "highlightTexture"
                :s-smoothness  "highlightSmoothness"
                :s-shine       "highlightSkinShine"}))])
      [:li.menu-item
       [:button.btn.input-group-btn.btn-primary
        {:on-click (fn [] (switch-tab :tab/order-cream))}
        "Continue"]])
     (when (debug?)
       [:div
        [:pre.code {:data-lang "json"}
         [:code (js/JSON.stringify (clj->js (:results @app-state)) nil 3)]]])]
    [:div.column.col-8 [:div#afterContainer]]]])

(defn results-tab
  []
  (reagent/create-class
   {:display-name        "results-tab"
    :component-did-mount (fn [_]
                           (apply-highlight-method "reset"))
    :reagent-render      render-results-tab}))

(defn toggle-show-settings []
  (swap! app-state assoc :show-settings
         (not(:show-settings @app-state))))

;;TODO refactor this mess
(defn face-panel []
  [:div.panel {:style {:height "100%"}}
   [:div.panel-header
    [:div.p-absolute [i18n/lang-menu
                      {:en "English"
                       :he "עברית"
                       :fr "Francais"}]]]
   [:div.panel-title.text-center
    [:p.show-sm [:u "ORLAB"]]
    [:img.hide-sm {:src "images/logo_orlab.svg" :width 175
            :alt (tr [:orlab "OrLab"])}]
    (when (not-empty (:errors @app-state))
      [:div.toast.toast-error
       [:button.btn.btn-clear.float-right {:on-click (fn[] (swap! app-state assoc :errors []))}]
       [:p [:strong  "An error occurred. Please try again, or contact support. "]]
       (map (fn [error] [:div {:key (str "error-" error)} error]) (:errors @app-state))])]
   [:nav.panel-nav
    [:ul.step {:class (when (:analyzing @app-state) "loading")}
     (doall (map
             tab-button
             [:tab/login
              :tab/personal-info
              :tab/customize-cream
              :tab/scan
              :tab/analysis
              :tab/order-cream
              :tab/done]))]]
   [:div.panel-body {:dir (tr [:text-dir "ltr"])}
    (cond
      (selected-tab? :tab/login) [:div#login
                               (login/form {:on-submit (submit-and-next-tab :login @login/form-state :tab/personal-info)})]
      (selected-tab? :tab/personal-info) [:div#personal-info
                                       (personal-info/form {:on-submit (submit-and-next-tab :personal-info @personal-info/form-state :tab/customize-cream)})]
      (selected-tab? :tab/customize-cream) [:div#skin-survey
                                       (skin-survey/form {:on-submit (submit-and-next-tab :skin-survey @skin-survey/form-state :tab/scan)})]

      (selected-tab? :tab/analysis) [results-tab]
      (selected-tab? :tab/scan) (choose-image-tab)
      (selected-tab? :tab/order-cream) [:div
                                        (when (debug?)
                                          [:ul
                                           (map (fn [[k v]] ^{:key k}[:li [:strong k] " - " v])
                                                (:cream-ingredients @app-state))])
                                        [:div.text-center
                                         [:ul.step {:class (if-not (get-active-step) "d-none")}
                                          [step-item "place" "Place Base Cream"]
                                          [step-item "dosing" "Dosing"]
                                          [step-item "mix" "Mixing"]
                                          [step-item "done" "Done"]]
                                         [:button.btn.input-group-btn.text-center
                                          {:on-click (fn [] (driver/start-process
                                                             (:cream-ingredients @app-state)
                                                             (:machine-url @app-settings)
                                                             update-status))
                                           :class (if-not (contains? #{"idle" nil} (get-active-step)) "d-none")}
                                          "Start"]
                                         (case (get-active-step)
                                           "place" [:div (tr ["Please place the base cream in the device."])]
                                           "dosing" [:div
                                                     [:div.loading.loading-lg]
                                                     [:div (tr ["Adding active product."])]]
                                           "done" (do (switch-tab :tab/done)
                                                      [:button.btn.input-group-btn.text-center
                                                       {:on-click (fn [] (update-status {}))}
                                                       "Reset"])
                                           [:div ])
                                         (when (debug?)
                                           [:div ((@app-state :pump-status) :status)]
                                           [:div [:samp (goog.string.format "%5.2f" (or ((@app-state :pump-status) :load) 0))]]
                                           [:div ((@app-state :pump-status) :device)])]
                                        (when (debug?)
                                          [:pre.code {:data-lang "json"}
                                           [:code (js/JSON.stringify (clj->js (:pump-status @app-state)) nil 3)]])]
      (selected-tab? :tab/done) [:div.empty
                                 [:div.empty-icon [:i.icon.icon-check.icon-3x]]
                                 [:p.empty-title.h5 (tr [:done-message "Thank You!"])]
                                 [:p.empty-subtitle (tr [:done-long-message "You will receive an email with details."])]
                                 [:div.empty-action
                                  (reset-button)]])]
   [:div.panel-footer.btn-group
    [:button.btn.btn-sm
     {:on-click (fn []
                  (let [tab-history (:tab-history @app-state)
                        prev-tab (last tab-history)]
                    (when (not-empty tab-history)
                      (switch-tab prev-tab)
                      (swap! app-state assoc :tab-history (pop tab-history)))))}
     [:i.icon.icon-back]]
    [reset-button {:class "btn-sm"}]
    (when (admin?)
      [:button.btn.btn-sm {:on-click toggle-show-settings} "⚙"])
    (when (admin?)
      [:button.btn.btn-sm
       {:class (when (:debug @app-state)"active")
        :on-click (fn []
                    (swap! app-state assoc :debug (not (debug?)))
                    (js/console.log (clj->js (formula/make-normalized-formula (:formula-ingredients @app-state) {} 10 )))
                    (js/console.log (clj->js @app-state))
                    (js/console.log (.-methods revieve.AR)))} "debug"])]
   [:div (when (:show-settings @app-state)
      (settings/form {} app-settings))]])

  

(defn mount [el]
  (reagent/render [face-panel] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (mount-app-element)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
