(ns cljs-forms.revieve)

;; using the sandbox id
(defonce revieve (let
                     [revieve (new js/RevieveSDK "XxDi4W65d2")]
                   (.setConfiguration
                    revieve
                    #js{:components
                        #js["eyes",
                            "wrinkles",
                            "redness",
                            "hyperpigmentation",
                            "texture",
                            "smoothness",
                            "makeup",
                            "masks",
                            "wrinkles_visualization",
                            "hyperpigmentation_visualization",
                            "shine",
                            "shine_visualization"]})
                   revieve ))

;; (def revieve (new js/RevieveSDK "VweadehMqO"))
(def CV (.-CV revieve))
(def AR (.-AR revieve))

(defn analyze [done-cb error-cb]
  (.catch
   (.then (.analyzeImage (.-CV revieve)) done-cb)
   error-cb))

(defn set-image [image call-back]
  (.then (.setImage (.-CV revieve) image) call-back))

(defn get-image
  "Get the image that was sent to revive as base64 string"
  []
  (.getImage CV))
