(ns cljs-forms.image-input
  (:require [oops.core :refer [oget oset! ocall oapply ocall! oapply!
                               oget+ oset!+ ocall+ oapply+ ocall!+ oapply!+]]))

(defn update-image [state image-key new-image]
  (let [reader (js/FileReader.)]
    (oset! reader "onload" (fn [e]
                             (let [data-url (oget e "target.result")]
                               (swap! state assoc image-key data-url))))
    (ocall reader "readAsDataURL" new-image)))

(defn upload-image [uploader state image-key new-image]
  (let [img-url (oget (uploader new-image (oget new-image "name")
                                {:handler (fn [response]
                                            (.log js/console  response))}) "lastUri_")]
    (swap! state assoc image-key img-url)))

(defn image-input [options state state-key]
  (if (= "" (get @state state-key ""))
    [:input.form-input.input-sm
     {:type "file"  :accept "image/*" :capture "user"
      :on-change (fn [event]
                   (let [first-file (.item (.-files (.-target event)) 0)]
                     (if (get options :uploader)
                       (upload-image (options :uploader) state state-key first-file)
                       (update-image state state-key first-file))))}]
    [:div.s-rounded [:img.img-responsive.s-rounded
                     {:src (get @state state-key)
                      :width 250
                      :on-click (fn []
                                  (when (js/confirm "Remove image?")
                                    (swap! state dissoc state-key)))}]]))

