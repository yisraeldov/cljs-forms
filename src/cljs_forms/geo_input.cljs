(ns cljs-forms.geo-input
  (:require ajax.core))

(defn fetch-geoip-name []
  (let [form-state (atom {})]
    (ajax.core/GET "https://freegeoip.app/json/"
                   {:keywords?       true
                    :response-format :json
                    :handler
                    (fn [location-data]
                      (print location-data)
                      (swap! form-state assoc
                             :location (str
                                        (:city location-data) ", "
                                        (:region_name location-data) ", "
                                        (:country_name location-data))))})))

(fetch-geoip-name)

(defn fetch-geo-names [name call-back]
  (ajax.core/GET (str "http://api.geonames.org/searchJSON?name=" name "&maxRows=10&username=max45&featureClass=P")
    {:keywords? true
     :response-format :json
     :handler (fn [response] (call-back  (:geonames response)))}))

(defn format-geo-name [geoname]
  (str (:name geoname) ", " (:adminName1 geoname) ", " (:countryName geoname)))

(defn geo-names-to-options [geonames]
  (clj->js (map (fn [geoname] {:value (format-geo-name geoname) :label (format-geo-name geoname)}) geonames)))
