(ns cljs-forms.driver
  (:require [ajax.core :refer [GET POST]]))

;; TODO don't hard-code the api url
;;(def api-url "http://127.0.0.1:5000/")
;;this is for the dev pi
(def api-url "http://172.27.138.64:5000/")
(defn get-status[]
  "place")

(defn poll [update-status-method api-url]
  (ajax.core/GET api-url
    :response-format :json
    :keywords? true
    :handler (fn [status] (update-status-method status)
               (when (not= "done" (status :status))
                 (js/setTimeout
                  (fn []
                    (poll update-status-method api-url)) 200)))))

(defn start-polling [update-status-method api-url]
  (GET (str api-url "start")
       :response-format :json
       :keywords? true
       :handler update-status-method)
  (poll update-status-method api-url))

(defn send-formula [formula api-url handler]
  (POST (str api-url "formula")
        {:response-format :json
         :format :json
         :params formula
         :handler handler}))

(defn send-personal-info [personal-info api-url handler]
  (POST (str api-url "user_info")
        {:response-format :json
         :format :json
         :params personal-info
         :handler handler}))

(defn send-app-state [app_state api-url handler]
  (POST (str api-url "ui_state")
        {:response-format :json
         :format :json
         :params app_state
         :handler handler}))

(defn start-process [formula api-url update-status-method]
  (send-formula formula api-url
                (fn [] (start-polling update-status-method api-url))))
