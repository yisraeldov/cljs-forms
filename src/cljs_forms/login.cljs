(ns cljs-forms.login
  (:require
   [cljs-forms.form-components :refer [form-group, input-field, submit-button]]
   [reagent.core :as reagent :refer [atom]]))

(defonce form-state (atom {}))
(defn form
  ([] (form {}))
  ([options]
   [:form
    (merge {:on-submit (fn [event]
                         (.preventDefault  event)
                         (print "submited")
                         (print event)
                         false)}
           options)
    (form-group {:label :operator-id
                 :required true
                 :type "text"
                 :autoComplete "off"
                 :autoCorrect "off"
                 :inputMode "text"
                 :autoCapitalize "none"
                 :spellCheck "false"
                 :name "new-password"}
                input-field
                form-state :operator-id)

    (submit-button :next)]))
